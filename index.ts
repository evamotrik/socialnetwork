import express from 'express'
import allRoutes from './src/routes/allRoutes'
import db from './src/db/db'
import * as bodyParser from 'body-parser'
import  chatController from './src/controllers/chatController'

const app = express()

const parser = express.json()
const urlencodedParser = bodyParser.urlencoded({ extended: true })

app.use(parser)
app.use(urlencodedParser)
app.use('/', allRoutes)

const PORT = 3000
const socket = require("socket.io");

const server = app.listen(PORT, () => {
    db
    console.log(`Sever is running on port:  ${PORT}`)
})

const io: any = socket(server)


io.on("connection",(socket:any)=> {
    console.log("ura")
    socket.on("sendMessage", async(message:any) =>{
        const messages = await chatController.sendMessage(message)
        io.emit('tweet', messages);
    })
})

