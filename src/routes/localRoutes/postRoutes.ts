import * as express from 'express'
import postController from '../../controllers/postController'
import authMiddleware from '../../middleware/authMiddleware'

const router = express.Router()

router.post('/create', postController.addNewPost)
router.get('/getMyPosts', authMiddleware, postController.getMyPosts) 
router.delete('/delete', authMiddleware, postController.deletePost) 

export default router

