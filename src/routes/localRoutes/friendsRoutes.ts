import * as express from 'express'
import friendsController from '../../controllers/friendsController'
import authMiddleware from '../../middleware/authMiddleware'

const router = express.Router()

router.post('/addFriend', authMiddleware, friendsController.addFriend)
router.post('/sendInvitation', authMiddleware, friendsController.sendInvitation)
router.get('/showInvitation', authMiddleware, friendsController.showInvitation)
router.delete('/deleteInvitation', authMiddleware, friendsController.deleteInvitation)
router.get('/showAllFriends', authMiddleware, friendsController.showAllFriends)
router.delete('/delete', authMiddleware, friendsController.deleteFromFriends)

export default router

