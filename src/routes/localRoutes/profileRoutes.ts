import * as express from 'express'
import profileController from '../../controllers/profileController'
import authMiddleware from '../../middleware/authMiddleware'

const router = express.Router()

router.post('/changeAvatar', authMiddleware, profileController.changeAvatar)

export default router

