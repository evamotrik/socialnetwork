import * as express from 'express'
import chatController from '../../controllers/chatController'
import authMiddleware from '../../middleware/authMiddleware'

const router = express.Router()

router.post('/getChat', authMiddleware, chatController.getChat) 

export default router

