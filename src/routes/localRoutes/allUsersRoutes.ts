import * as express from 'express'
import allUsersController from '../../controllers/allUsersController'
import authMiddleware from '../../middleware/authMiddleware'

const router = express.Router()

router.get('/getAllUsers', allUsersController.getAllUsers)
router.post('/likePost',authMiddleware, allUsersController.likePost)
router.post('/addComment',authMiddleware, allUsersController.addComment)
router.post('/showPostComments',authMiddleware, allUsersController.showPostComments)
router.delete('/deleteComment',authMiddleware, allUsersController.deleteComment)

export default router

