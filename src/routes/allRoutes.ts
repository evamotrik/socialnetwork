import * as express from 'express'
import authRoutes from "./localRoutes/authRoutes"
import postRoutes from "./localRoutes/postRoutes"
import profileRoutes from "./localRoutes/profileRoutes"
import allUsersRoutes from "./localRoutes/allUsersRoutes"
import friendsRoutes from "./localRoutes/friendsRoutes"
import chatRoutes from "./localRoutes/chatRoutes"

const router = express.Router()

router.use('/auth', authRoutes);
router.use('/post', postRoutes);
router.use('/profile', profileRoutes);
router.use('/allUsers', allUsersRoutes);
router.use('/friends', friendsRoutes);
router.use('/chat', chatRoutes);

export default router


