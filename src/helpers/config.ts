const secret = "SECRET_KEY"
const mongoUri = "mongodb://localhost:27017/SocialNetWork"

export {
    secret, 
    mongoUri
}
