import * as jwt from 'jsonwebtoken'
import {secret} from './config'

const generateAccessToken = (id: any) =>{
    const payload = {
        id
    }
    return jwt.sign(payload, secret, {expiresIn: "24h"})
}

export default generateAccessToken