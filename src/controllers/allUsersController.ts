import { Post } from "../models/Post";
import { User } from "../models/User";
import { Comment } from "../models/Comment";

class AllUsersController {

    async getAllUsers(req: any, res: any) {
        try {
            const users = await User.find()

            res.json(users)
        } catch (e) {
            res.status(400).json({ message: "Showing all users error -> " + e })
        }
    }

    async likePost(req: any, res: any) {
        try {
            const post = await Post.findOne({ _id: req.body.postId });
            if (post) {
                const isAlreadyLiked = post.likes.indexOf(req.user.id);
                if (isAlreadyLiked === -1) {
                    await Post.updateOne({ _id: req.body.postId }, { likes: [...post.likes, req.user.id] })
                } else {
                    await Post.updateOne({ _id: req.body.postId }, { likes: post?.likes.filter((item: any) => item != req.user.id) })
                }
                return res.status(200).send('Like on post')
            }

        } catch (e) {
            console.log(e)
        }
    }

    async addComment(req: any, res: any) {
        try {
            const { commentText, postId, userId, userAvatar, userSurname, userName } = req.body
            let comment = await Comment.create({ commentText, postId, userId, userAvatar, userSurname, userName });
            let postFind = await Post.findById(postId);
            if (postFind) {
                postFind.comments.push(comment._id)
                await postFind.save()
            }
            res.json({ comment })
        } catch (e) {
            res.status(400).json({ message: "Adding comment error -> " + e })
        }
    }

    async showPostComments(req: any, res: any) {
        try {
            const post = await Post.findOne({ _id: req.body.postId });
            res.json(post?.comments)
        } catch (e) {
            res.status(400).json({ message: "Show comments error -> " + e })
        }
    }

    async deleteComment(req: any, res: any) {
        try {
            const { postId, commentId } = req.body
            await Post.updateOne({ _id: postId }, { $pull: { comment: commentId } });
            await Comment.deleteOne({ _id: commentId })
            res.status(200).send(`Delete comment`)
        } catch (e) {
            res.status(400).json({ message: "Delete comment error -> " + e })
        }
    }
}

export default new AllUsersController
