import { Chat } from "../models/Chat"
import { Message } from "../models/Message"

class ChatController {

    async sendMessage(mes: any) {
        try {
            let findChat = await Chat.find({ members: { $all: [mes.userId, mes.friendId] } })
            let chatId = findChat[0]._id
            let message = await Message.create({ userId: mes.userId, userAvatar: mes.userAvatar, userSurname: mes.userSurname, userName: mes.userName, chatId: chatId, messageText: mes.messageText });
            let chat = await Chat.findById(chatId);
            if (chat) {
                chat.messages.push(message._id)
                await chat.save()
            }
            const messages = await Message.find({_id: chat?.messages})
            return messages;
        } catch (e) {
            console.log(e)
        }
    }   

    async getChat(req: any, res: any){
        try {
            let findChat = await Chat.find({ members: { $all: [req.user.id, req.body.friendId] } })
            let chatId = findChat[0]._id
            let chat = await Chat.findById(chatId);
            res.json(chat)
        } catch (e) {
            console.log(e)
        }
    }
    
}

export default new ChatController;
