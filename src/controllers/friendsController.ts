import { User } from "../models/User";
import { Chat } from "../models/Chat";

class FriendsController {

    async addFriend(req: any, res: any) {
        try {
            let user = await User.findById(req.user.id)
            let friend = await User.findById(req.body.friendsId);

            user?.friends.push(friend?._id)
            await user?.save();

            friend?.friends.push(user?._id)
            await friend?.save();

            await User.updateOne({ _id: req.user.id }, { $pull: { invitations: req.body.friendsId } })

            let chat = await Chat.create({
                members: [req.user.id, req.body.friendsId],
                messages: []
            });

            user?.chats.push(chat._id)
            await user?.save()

            friend?.chats.push(chat._id)
            await friend?.save()

            res.json({ message: `Пользователь добавлен в друзья` });

        } catch (e) {
            res.status(400).json({ message: "Ошибка добавления в друзья" })
        }
    }

    async sendInvitation(req: any, res: any) {
        try {
            let user = await User.findById(req.user.id)
            let friend = await User.findById(req.body.friendsId);

            friend?.invitations.push(user?._id)
            await friend?.save();

            res.json({ message: `Invitation for friendship is sended` });

        } catch (e) {
            res.status(400).json({ message: "Error with invitation" })
        }
    }

    async deleteInvitation(req: any, res: any) {
        try {
            await User.updateOne({ _id: req.user.id }, { $pull: { invitations: req.body.friendsId } })
            res.status(200).send(`Invitation deleted`);
        } catch (e) {
            res.status(400).json({ message: "Error with invitation delete" })
        }
    }

    async showInvitation(req: any, res: any) {
        try {
            let user = await User.findById(req.user.id)
            res.json(user?.invitations)

        } catch (e) {
            res.status(400).json({ message: "Error with showing invitation" })
        }
    }

    async showAllFriends(req: any, res: any) {
        try {
            let user = await User.findById(req.user.id)
            res.json(user?.friends)

        } catch (e) {
            res.status(400).json({ message: "Error with showing friends" })
        }
    }

    async deleteFromFriends(req: any, res: any) {
        try {
            await User.updateOne({ _id: req.user.id }, { $pull: { friends: req.body.friendId } })
            await User.updateOne({ _id: req.body.friendId }, { $pull: { friends: req.user.id } })

            let findChat = await Chat.find({ members: { $all: [req.user.id, req.body.friendId] } })
            let chatId = findChat[0]._id
            await User.updateOne({ _id: req.user.id }, { $pull: { chats: chatId } })
            await User.updateOne({ _id: req.body.friendId }, { $pull: { chats: chatId } })
            await Chat.deleteOne({ _id: chatId })

            res.status(200).send(`Friend deleted`);
        } catch (e) {
            res.status(400).json({ message: "Error with deleting user from friends" })
        }
    }

}

export default new FriendsController;

