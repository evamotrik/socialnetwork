import { Post, Ipost } from "../models/Post"
import { Request, Response } from 'express'
import { User } from "../models/User"
import { Comment } from "../models/Comment"

class postController {
    async addNewPost(req: Request<Ipost>, res: Response) {
        try {
            const { title, descr, img, video, user } = req.body
            let post = await Post.create({ title, descr, img, video, user });
            let userFind = await User.findById(user);
            if (userFind) {
                userFind.posts.unshift(post._id)
                await userFind.save()
            }
            res.json({ post });
        } catch (e) {
            res.status(400).json({ message: "Adding post error -> " + e })
        }
    }

    async getMyPosts(req: any, res: any) {
        try {
            let user = await User.findById(req.user.id)

            if (user) {
                const posts = user.posts
                res.json({ posts })
            }

        } catch (e) {
            res.status(400).json({ message: "error -> " + e })
        }

    }

    async getAllPosts(req: any, res: any) {
        try {
            const posts = await Post.find()
            res.json({ posts })
        } catch (e) {
            res.status(400).json({ message: "error -> " + e })
        }
    }

    async deletePost(req: any, res: any) {
        try {
            await User.updateOne({ _id: req.user.id }, { $pull: { posts: req.body.postId } });
            await Comment.deleteMany({ postId: req.body.postId });
            await Post.deleteOne({ _id: req.body.postId })
            res.status(200).send(`Post deleted`);
        } catch (e) {
            res.status(400).json({ message: "Deleting post error -> " + e })
        }
    }

}

export default new postController
