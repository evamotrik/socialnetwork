import { User } from "../models/User";

class ProfileController {
    async changeAvatar(req: any, res: any){
        try{
            const user = await User.findById(req.user.id)
            
            if (user){
                const {avatar} = req.body;
                await User.updateOne({_id: user._id},{avatar:avatar})
                res.json({message:"Avatar changed"})
            }
        } catch(e) {
            res.status(400).json({ message: "Changing avatar error -> " + e })
        }      
    }
}

export default new ProfileController;
 