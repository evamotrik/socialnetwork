import mongoose from "mongoose"
const { model, Schema } = mongoose
import autopopulate from 'mongoose-autopopulate'

interface Imessage extends mongoose.Document {
    messageText: string;
    userId: string;
    userAvatar: string;
    userSurname: string;
    userName: string;
    chatId: mongoose.Types.ObjectId;
}

let messageSchema = new Schema({
    messageText: {
        type: String,
        required: true
    },
    userId: {
        type: String,
    },
    userAvatar: {
        type: String,
    },
    userSurname: {
        type: String,
    },
    userName: {
        type: String,
    },
    chatId: {
        type: mongoose.Types.ObjectId,
        required: true,
        ref: 'Chat',
    }
})

messageSchema.plugin(autopopulate);
const Message = mongoose.model<Imessage>('Message', messageSchema)

export { Message, Imessage }
