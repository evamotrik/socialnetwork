import mongoose from "mongoose"
import { Ipost } from "./Post"
const {model, Schema} = mongoose
import autopopulate from 'mongoose-autopopulate'

interface Iuser extends mongoose.Document {
   email: string;
   password: string;
   name: string;
   surname: string;
   avatar: string,
   friends: Array<mongoose.Types.ObjectId>,
   posts: Array<mongoose.Types.ObjectId>,
   invitations: Array<mongoose.Types.ObjectId>,
   chats: Array<mongoose.Types.ObjectId>
  }

let userSchema = new Schema({
    email:{
        type: String,
        unique: true,
        required: true,
    },
    password:{
        type: String,
    },
    name:{
        type: String,
        required: true,
    },
    surname:{
        type: String,
        required: true,
    }  ,
    avatar: {
        type: String,
        required: true,
        default: 'https://uploads-ssl.webflow.com/5e4627609401e01182af1cce/5eb13bfdb4659efea4f8dace_profile-dummy.png'
    },
    posts:[{
        type: mongoose.Types.ObjectId,
        ref: 'Post',
        autopopulate:true
    }],
    friends:[{
        type: mongoose.Types.ObjectId,
        ref: 'User',
        autopopulate: true
    }],
    invitations: [{
        type: mongoose.Types.ObjectId,
        ref: 'User',
        autopopulate: {
            select: '_id name surname avatar'
        }
    }],
    chats: [{
        type: mongoose.Types.ObjectId,
        ref: 'Chat'
    }],
})

userSchema.plugin(autopopulate);
const User = mongoose.model<Iuser>('User', userSchema)

export {User, Iuser}
