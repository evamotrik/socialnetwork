import mongoose from "mongoose"
const { model, Schema } = mongoose
import autopopulate from 'mongoose-autopopulate'

interface Icomment extends mongoose.Document {
    commentText: string;
    userId: string;
    userAvatar: string;
    userSurname: string;
    userName: string;
    postId: string;
}

let commentSchema = new Schema({
    commentText: {
        type: String,
        required: true
    },
    userId: {
        type: String,
    },
    userAvatar: {
        type: String,
    },
    userSurname: {
        type: String,
    },
    userName: {
        type: String,
    },
    postId: {
        type: String
    }

})

commentSchema.plugin(autopopulate);
const Comment = mongoose.model<Icomment>('Comment', commentSchema)

export { Comment, Icomment }
