import mongoose from "mongoose"
const { model, Schema } = mongoose
import autopopulate from 'mongoose-autopopulate'

interface Ichat extends mongoose.Document {
    members: Array<mongoose.Types.ObjectId>;
    messages: Array<mongoose.Types.ObjectId>;
}

let chatSchema = new Schema({
    members:[{
        type: mongoose.Types.ObjectId,
        ref: 'User',
        autopopulate: {maxDepth: 1}
    }],
    messages: [{
        type: mongoose.Types.ObjectId,
        ref: 'Message',
        autopopulate:{maxDepth:1}
    }]
   
})

chatSchema.plugin(autopopulate);
const Chat = mongoose.model<Ichat>('Chat',chatSchema)

export { Chat, Ichat }
