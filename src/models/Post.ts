import mongoose from "mongoose"
const { model, Schema } = mongoose
import autopopulate from 'mongoose-autopopulate'

interface Ipost extends mongoose.Document {
    title: string;
    img: string;
    video: string;
    descr: string;
    user: mongoose.Types.ObjectId;
    likes: [mongoose.Types.ObjectId],
    comments: [mongoose.Types.ObjectId],
}

let postSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    img: {
        type: String
    },
    video: {
        type: String,
        default:""

    },
    descr: {
        type: String,
        required: true
    },
    user: {
        type: mongoose.Types.ObjectId,
        required: true,
        ref: 'User',
    },
    likes: [{
        type: mongoose.Types.ObjectId,
        ref: 'User'
    }], 
    comments: [{
        type: mongoose.Types.ObjectId,
        ref: 'Comment',
        autopopulate: {
            maxDepth: 1
        }
    }],
   
})

postSchema.plugin(autopopulate);
const Post = mongoose.model<Ipost>('Post', postSchema)

export { Post, Ipost }
