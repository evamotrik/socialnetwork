import {Request} from 'express'

import * as jwt from 'jsonwebtoken'
import {secret} from '../helpers/config'
import { Iuser } from '../models/User'

interface RequestUser extends Request{
    user: Iuser
}

const authMiddleware = (req: RequestUser, res: any, next: any)=>{
    if(req.method === "OPTIONS"){
        next()
    }
    try{
        const authHeaders = req.headers.authorization
        if(!authHeaders){
            return res.status(403).json({message: "Пользователь не авторизован"}) 
        }
        const token = authHeaders.split(" ")[1]
        if(!token){
            return res.status(403).json({message: "Пользователь не авторизован"}) 
        }
        const decodedData = jwt.verify(token, secret)
        req.user = decodedData as Iuser
        next()
    }catch(e){
        return res.status(403).json({message: "Пользователь не авторизован"})
    }
}

export default authMiddleware