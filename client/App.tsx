import React, { useReducer } from 'react';
import Context from "./src/utils/context";
import * as ACTIONS from "./src/actionCreators/user.action";
import { ReducerFunction, defaultState } from "./src/reducers/user.reducer";

import StackNavigator from './src/navigation/StackNavigation';

export default function App() {

  const [stateUser, dispatchUserReducer] = useReducer(ReducerFunction, defaultState);
  const setData = (data: any) => {
    dispatchUserReducer(ACTIONS.addUser(data));
  };

  return (
    <Context.Provider
      value={{
        userState: stateUser,
        setData: (data: any) => setData(data)
      }}>  
      <StackNavigator />
    </Context.Provider>
  );
}