import React, { useEffect, useState, useContext } from 'react';
import { ImageBackground, Text, StyleSheet, View, Image, ScrollView, TouchableOpacity, Modal, Button } from 'react-native';
import { showAllFriends, likePost, deleteFromFriends } from '../services/http.service';
import { Video } from 'expo-av';
import Context from '../utils/context';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { ModalComments } from './components/ModalComments';

export const Main = ({ navigation }) => {
    const context = useContext(Context);
    const [visibleComments, setVisibleComments] = React.useState(false);
    const ownerId = context.userState._id
    const [friends, setFriends] = useState([]);
    const [postIdCommentModal, setPostIdCommentModal] = useState("");
    const [visibleDeleteFriend, setVisibleDeleteFriend] = React.useState(false);
    const [friendId, setFriendId] = useState("");

    const vid = React.useRef(null);
    const [status, setStatus] = React.useState({});

    const showModalComments = (postId: any) => {
        setVisibleComments(true);
        setPostIdCommentModal(postId);
    }

    const deleteFriend = async () => {
        await deleteFromFriends({ friendId })
        hideModalDeleteFriend();
        const friendsArr = await showAllFriends();
        setFriends(friendsArr);
    }

    const showModalDeleteFriend = (friendId: any) => {
        setVisibleDeleteFriend(true);
        setFriendId(friendId);
    };

    const hideModalDeleteFriend = async () => {
        const friendsArr = await showAllFriends();
        setFriends(friendsArr);
        setVisibleDeleteFriend(false);
    }

    useEffect(() => {
        (async () => {
            const friendsArr = await showAllFriends();
            setFriends(friendsArr);
        })()
    }, [])


    const toLikePost = async (postId: any) => {
        await likePost({ postId })
        const friendsArr = await showAllFriends();
        setFriends(friendsArr);
    }

    return (
        <View style={styles.view}>
            {friends.length === 0 ?
                <ImageBackground source={require('../../assets/headerPic.jpg')} style={styles.imageBackground}>
                    <View style={styles.noFriendsView}>
                        <Text style={styles.noFriends}> You don't have news</Text>
                        <Text> Because you don't have friends yet</Text>
                        <TouchableOpacity style={styles.btnFindFriends} onPress={() => navigation.navigate('Users')}>
                            <Text style={styles.btnFindFriendsText}>Find friends</Text>
                        </TouchableOpacity>
                    </View>
                </ImageBackground>
                :
                <View style={styles.view}>
                    <ScrollView horizontal={true} style={styles.horizontalScroll}>
                        <ImageBackground source={require('../../assets/headerPic.jpg')} style={styles.backgroundImage}>
                            {friends.map(friend => (
                                <View key={friend._id} style={styles.friendView}>
                                    <Image
                                        style={styles.friendsAvatar}
                                        source={{ uri: friend.avatar }} />
                                    <View style={styles.friendInfoText}>
                                        <Text style={styles.horizontalScrollText}>{friend.surname} </Text>
                                        <Text style={styles.horizontalScrollText}>{friend.name}</Text>
                                    </View>
                                    <View style={styles.friendInfoIcons}>
                                        <TouchableOpacity onPress={() => navigation.navigate('Chat', { userId: context.userState.userId, friendId: friend._id })}>
                                            <Ionicons name="chatbubbles" size={25} color='black' />
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => showModalDeleteFriend(friend._id)}>
                                            <Ionicons name="close-circle" size={25} color='black' />
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            ))}
                        </ImageBackground>
                    </ScrollView>
                    <View style={styles.textFriendsPostsView}>
                        <Text style={styles.textFriendsPosts}>Friends Posts</Text>
                    </View>
                    <ScrollView >
                        <View style={styles.friendsPosts}>
                            {friends.map(friend => (
                                <View key={friend._id} style={styles.posts}>
                                    {
                                        friend.posts.map(post => (
                                            <View key={post._id} style={styles.post}>
                                                <View style={styles.postInfoAboutAuthor}>
                                                    <Image
                                                        style={styles.postFriendsAvatar}
                                                        source={{ uri: friend.avatar }} />
                                                    <Text style={styles.postInfoAboutAuthorText}>{friend.surname} {friend.name}</Text>
                                                </View>
                                                {post.video ?
                                                    <View>
                                                        <Video
                                                            ref={vid}
                                                            style={styles.postPic}
                                                            source={{ uri: post.video }}
                                                            useNativeControls
                                                            resizeMode="contain"
                                                            isLooping
                                                            onPlaybackStatusUpdate={status => setStatus(() => status)} />
                                                    </View>
                                                    : null}
                                                {post.img ?
                                                    <Image
                                                        style={styles.postPic}
                                                        source={{ uri: post.img }} /> : null}
                                                <View>
                                                    <Text style={styles.postInfoAboutPostTitle}>{post.title}</Text>
                                                    <Text style={styles.postInfoAboutPostDescr}>{post.descr}</Text>
                                                </View>
                                                <View style={styles.likesComments}>
                                                    <TouchableOpacity onPress={() => toLikePost(post._id)}>
                                                        {post.likes.includes(ownerId) == true ?
                                                            <Ionicons name="heart-circle" size={30} color='black' />
                                                            : <Ionicons name="heart-circle-outline" size={30} color='black' />
                                                        }

                                                    </TouchableOpacity>
                                                    <Text style={styles.likesNumb}>{post.likes.length}</Text>
                                                    <TouchableOpacity onPress={() => showModalComments(post._id)}>
                                                        <Ionicons name="chatbubble-outline" size={30} color='black' />
                                                    </TouchableOpacity>
                                                </View>
                                                {visibleComments ? <ModalComments postId={postIdCommentModal} setVisibleComments={setVisibleComments} /> : null}
                                            </View>
                                        ))
                                    }
                                </View>
                            ))}
                        </View>
                    </ScrollView>


                    <Modal visible={visibleDeleteFriend}
                        onDismiss={hideModalDeleteFriend}
                        animationType="slide"
                        transparent={true}>
                        <View style={styles.modalDeleteFriendBackground}>
                            <View style={styles.modalDeleteFriend}>
                                <TouchableOpacity onPress={hideModalDeleteFriend} style={styles.modalClose}>
                                    <Ionicons name="close" size={30} color='black' />
                                </TouchableOpacity>
                                <Text style={styles.modalText}>Are you sure you want to delete this friend?</Text>
                                <View style={styles.modalDeleteBtns}>
                                    <TouchableOpacity onPress={deleteFriend} style={styles.modalDeleteFriendYes}>
                                        <Text>Yes</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={hideModalDeleteFriend} style={styles.modalDeleteFriendNo}>
                                        <Text style={styles.modalDeleteFriendNoText}>No</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </Modal>
                </View>
            }
        </View>
    )
}

const styles = StyleSheet.create({
    view: {
        flex: 1,
        alignItems: 'center',
    },
    imageBackground: {
        height: '101%',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    noFriendsView: {
        backgroundColor: 'white',
        height: 500,
        width: 350,
        borderRadius: 50,
        alignItems: 'center',
        justifyContent: 'center'
    },
    noFriends: {
        fontSize: 20,
        fontWeight: 'bold'
    },
    btnFindFriends: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'black',
        height: 35,
        borderRadius: 50,
        width: 130,
        marginTop: 10,
    },
    btnFindFriendsText: {
        color: 'white'
    },
    horizontalScroll: {
        width: '100%',
        height: 190,
        paddingLeft: 60
    },
    backgroundImage: {
        flexDirection: 'row',
        width: 500,
        height: 155,
        alignItems: 'center',
        padding: 10
    },
    friendView: {
        margin: 10,
        marginTop: 0,
    },
    friendInfoText: {
        backgroundColor: 'white',
        height: 35,
        borderRadius: 30
    },
    friendInfoIcons: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginBottom: 15
    },
    friendInfo: {
        alignItems: 'center'
    },
    horizontalScrollText: {
        fontSize: 13,
        textAlign: 'center'
    },
    friendsAvatar: {
        margin: 10,
        marginBottom: 2,
        width: 60,
        height: 60,
        borderRadius: 100,
        borderColor: 'black',
        borderWidth: 3
    },
    textFriendsPostsView: {
        backgroundColor: 'black',
        width: 600,
        height: 30,
        alignItems: 'center',
        justifyContent: 'center',
    },
    textFriendsPosts: {
        color: 'white',
        fontWeight: 'bold'
    },
    friendsPostsScroll: {
        width: '100%',
        alignItems: 'center',
    },
    friendsPosts: {
        width: '100%',
        alignItems: 'center',
        marginBottom: 20
    },
    posts:{
        alignItems: 'center'
    },
    post: {
        margin: 25,
        marginTop: 0,
        alignItems: "flex-start",
    },
    postInfoAboutAuthor: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 6,
        marginBottom: 6,
        width: 300,
    },
    postInfoAboutAuthorText: {
        marginLeft: 5,
        fontWeight: 'bold',
    },
    postFriendsAvatar: {
        width: 30,
        height: 30,
        borderRadius: 100,
        borderColor: 'black',
        borderWidth: 2 / 5
    },

    postPic: {
        width: 300,
        height: 300,
        borderRadius: 30,
        alignSelf: 'center'
    },
    postInfoAboutPostTitle: {
        fontSize: 15,
        fontWeight: 'bold',
        marginTop: 6,
    },
    postInfoAboutPostDescr: {
        fontSize: 15
    },
    likesComments: {
        alignItems: 'center',
        flexDirection: 'row'
    },
    likesNumb: {
        fontSize: 15,
        marginRight: 5
    },
    modalDeleteFriendBackground: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: 'rgba(52, 52, 52, 0.8)',
        justifyContent: 'center'
    },
    modalDeleteFriend: {
        backgroundColor: 'white',
        height: 300,
        width: 350,
        borderRadius: 50,
        justifyContent: 'center',
        alignItems: 'center',
    },
    modalDeleteBtns: {
        flexDirection: 'row',
        marginTop: 10
    },
    modalDeleteFriendYes: {
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: 'black',
        borderWidth: 2,
        height: 35,
        borderRadius: 50,
        width: 70,
        marginBottom: 10,
        marginRight: 10
    },

    modalDeleteFriendNo: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'black',
        height: 35,
        borderRadius: 50,
        width: 70,
        marginBottom: 10
    },
    modalDeleteFriendNoText: {
        color: 'white'
    },
    modalClose: {
        position: 'absolute',
        right: 5,
        top: 5,
        margin: 15,
        alignSelf: 'flex-end'
    },
    modalText: {
        fontSize: 16
    },
});


