import React, { useEffect, useState, useContext } from 'react';
import { ImageBackground, Modal, TouchableOpacity, TextInput, ScrollView, Text, StyleSheet, View, Image, Button, Touchable } from 'react-native';
import { getAllUsers, showInvitation, sendInvitation, addFriend, showAllFriends, deleteInvitation } from '../services/http.service';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Context from '../utils/context';

export const Users = () => {

  const context = useContext(Context);

  const [users, setUsers] = useState([]);
  const [friends, setFriends] = useState([])
  const [invitors, setInvitors] = useState([]);
  const [inputValue, setInputValue] = useState('');
  const [visible, setVisible] = React.useState(false);
  const ownerId = context.userState._id
  const hideModal = async () => setVisible(false);
  const showModal = () => setVisible(true);

  const addToFriends = async (friendsId: any) => {
    await addFriend({ friendsId })
    const invites = await showInvitation();
    setInvitors(invites);
    const allUsers = await getAllUsers();
    setUsers(allUsers);
  }

  const cancelInvitation = async (friendsId: any) => {
    await deleteInvitation({ friendsId });
    const invites = await showInvitation();
    setInvitors(invites);
  }

  const inviteFriend = async (friendsId: any) => {
    await sendInvitation({ friendsId })
    showModal()
  }

  useEffect(() => {
    (async () => {
      const allUsers = await getAllUsers();
      const allFriends = await showAllFriends()
      const invites = await showInvitation();
      setUsers(allUsers);
      setInvitors(invites);
      setFriends(allFriends);
    })()
  }, [])

  const friendsId = friends.map(friend => friend._id)

  return (
    <View style={styles.view} >
      <ImageBackground source={require('../../assets/headerPic.jpg')} style={styles.imageBackground}>
        <TextInput style={styles.inputSearch} value={inputValue} placeholder='Search...' onChangeText={text => setInputValue(text)} />
        <ScrollView >
          {invitors.length > 0 ?
            <View style={styles.invitationsView}>
              <Text style={styles.invitationText}>Invitations</Text>
              {
                invitors.map(invitor => (
                  <View key={invitor._id} style={styles.user}>
                    <Image
                      style={styles.invitationUserAvatar}
                      source={{ uri: invitor.avatar }} />
                    <View style={styles.usersInfo}>
                      <Text style={styles.usersInfoText}>{invitor.surname}</Text>
                      <Text style={styles.usersInfoText}>{invitor.name}</Text>
                    </View>
                    <TouchableOpacity style={styles.iconAddToFriends} onPress={() => addToFriends(invitor._id)}>
                      <Ionicons name="person-add" size={20} color='black' />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.iconAddToFriends} onPress={() => cancelInvitation(invitor._id)}>
                      <Ionicons name="close" size={20} color='black' />
                    </TouchableOpacity>
                  </View>
                ))
              }
            </View> : null}

          <View style={styles.allUsers}>
            <Text style={styles.allUsersFont}>All users</Text>
            {
              users?.
                filter((user: any) => (user.name.toLowerCase() + ' ' + user.surname.toLowerCase()).
                  includes(inputValue.toLocaleLowerCase())).
                map(user => (
                  user._id == ownerId ? null :
                    <View key={user._id} style={styles.user}>
                      <Image
                        style={styles.userAvatar}
                        source={{ uri: user.avatar }} />
                      <View style={styles.usersInfo}>
                        <Text style={styles.fontSize}>{user.surname}</Text>
                        <Text style={styles.fontSize}>{user.name}</Text>
                      </View>
                      {
                        friendsId.includes(user._id) == true ?
                          <Ionicons name="people" size={20} color='black' />
                          :
                          <TouchableOpacity onPress={() => inviteFriend(user._id)} style={styles.sendRequestBtn}>
                            <Ionicons name="person-add" size={20} color='white' />
                          </TouchableOpacity>
                      }
                    </View>
                ))
            }
          </View>
        </ScrollView>
      </ImageBackground>

      <Modal
        visible={visible}
        onDismiss={hideModal}
        animationType="slide"
        transparent={true}>
        <View style={styles.modalBackground}>
          <View style={styles.modalFriendShipRequest}>
            <Text style={styles.modalText}>Request for friendship is sended</Text>
            <TouchableOpacity onPress={hideModal} style={styles.btnFriendRequest}>
              <Text style={styles.btnFriendRequestText}>OK</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>

    </View>
  )
}

const styles = StyleSheet.create({
  view: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  imageBackground: {
    height: '101%',
    width: '100%',
    alignItems: 'center'
  },
  inputSearch: {
    borderWidth: 2,
    width: 380,
    height: 40,
    borderColor: 'black',
    justifyContent: 'center',
    paddingLeft: 10,
    margin: 15,
    borderRadius: 5,
    backgroundColor: 'white'
  },
  invitationsView: {
    width: 350,
    margin: 10,
    backgroundColor: 'black',
    justifyContent: 'center',
    borderRadius: 50,
    padding: 7
  },
  invitationUserAvatar: {
    width: 80,
    height: 80,
    borderRadius: 100,
    borderColor: 'white',
    borderWidth: 2
  },
  invitationText: {
    color: 'white',
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold'
  },
  iconAddToFriends: {
    backgroundColor: 'white',
    height: 30,
    width: 30,
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'center',
    margin: 10
  },
  allUsers: {
    marginTop: 15,
    marginBottom: 15,
    backgroundColor: 'white',
    borderRadius: 50,
    padding: 10,
    borderWidth: 3,
    borderColor: 'black',
    width: 350,
  },
  allUsersFont: {
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold'
  },
  user: {
    flexDirection: 'row',
    alignItems: 'center',
    margin: 10,
    justifyContent: 'space-around'
  },
  usersInfo: {
    flexDirection: 'column',
    marginLeft: 10
  },
  usersInfoText: {
    color: 'white',
    fontSize: 16
  },
  userAvatar: {
    width: 80,
    height: 80,
    borderRadius: 100,
    borderColor: 'black',
    borderWidth: 2
  },
  sendRequestBtn: {
    borderRadius: 100,
    backgroundColor: 'black',
    height: 30,
    width: 30,
    alignItems: 'center',
    justifyContent: 'center'
  },
  fontSize: {
    fontSize: 16
  },
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
},
modalFriendShipRequest: {
  backgroundColor: 'white',
  height: 250,
  width: 300,
  borderRadius: 50,
  justifyContent: 'center',
  alignItems: 'center',
  },
  modalText:{
    fontSize: 16
},
btnFriendRequest: {
  justifyContent: 'center',
  alignItems: 'center',
  backgroundColor: 'black',
  height: 35,
  borderRadius: 50,
  width: 70,
  marginTop: 10
},
btnFriendRequestText: {
  color: 'white'
},
});


