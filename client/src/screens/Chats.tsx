import React, { useEffect, useState } from 'react';
import { Text, StyleSheet, View, Image, TouchableHighlight, ImageBackground, ScrollView } from 'react-native';
import { showAllFriends } from '../services/http.service';

export const Chats = ({ navigation }) => {

  const [chats, setChats] = useState([])

  useEffect(() => {
    (async () => {
      const chatsArr = await showAllFriends();
      setChats(chatsArr);
    })()
  }, [])

  return (
    <View style={styles.view} >
      <ImageBackground source={require('../../assets/headerPic.jpg')} style={styles.imageBackground}>
        <View style={styles.textYourChats}>
          <Text style={styles.textYourChatsFont}>Your Chats</Text>
        </View>
        <ScrollView>
          <View style={styles.allChats}>
            {chats.map(friend => (
              <View key={friend._id} style={styles.userChat}>
                <TouchableHighlight style={styles.chat} onPress={() => navigation.navigate('Chat', { friendId: friend._id })}>
                  <View style={styles.chatView}>
                    <Image
                      style={styles.friendsAvatar}
                      source={{ uri: friend.avatar }} />
                    <View style={styles.friendInfo}>
                      <Text style={styles.friendInfoText}>{friend.surname}</Text>
                      <Text style={styles.friendInfoText}>{friend.name}</Text>
                    </View>
                  </View>
                </TouchableHighlight>

              </View>
            ))
            }
          </View>
        </ScrollView>
      </ImageBackground>
    </View>
  )
}

const styles = StyleSheet.create({
  view: {
    flex: 1,
    justifyContent: 'center',
  },
  imageBackground: {
    height: '101%',
    width: '100%',
  },
  textYourChats: {
    alignItems: 'center',
    backgroundColor: 'rgba(255, 255, 255, 0.8)'
  },
  textYourChatsFont: {
    fontSize: 25,
    fontWeight: 'bold',
    color: 'black'
  },
  allChats: {
    alignItems: 'center'
  },
  chat: {
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'row',
    padding: 5,
    margin: 7,
    borderColor: 'black',
    borderWidth: 3,
    borderRadius: 20,
    backgroundColor: 'white',
    width: '85%',
  },
  chatView: {
    flexDirection: 'row',
  },
  friendsAvatar: {
    margin: 10,
    marginBottom: 2,
    width: 60,
    height: 60,
    borderRadius: 100,
    borderColor: 'black',
    borderWidth: 2
  },
  friendInfo: {
    justifyContent: 'center',
    marginLeft: 10,
  },
  friendInfoText:{
    fontSize: 17
  },
  userChat: {
    flexDirection: 'row'
  }
});


