import React, { useState } from 'react';
// import { ImageBackground, Text, TouchableOpacity, StyleSheet, TextInput, View } from 'react-native';
import firebase from "firebase/app";
// import * as firebase from 'firebase';
import { FirebaseRecaptchaVerifierModal, FirebaseRecaptchaBanner } from 'expo-firebase-recaptcha';
import { firebaseConfig } from '../services/firebaseConfig';

import {
  Text,
  View,
  TextInput,
  Button,
  StyleSheet,
  TouchableOpacity,
  Platform
} from 'react-native';

firebase.initializeApp(firebaseConfig);

if(!firebase.apps){
  console.log('error')
  firebase.initializeApp(firebaseConfig)
} else {
  console.log(`already connected`)
}

export const LoginWithPhone = () => {

  const recaptchaVerifier = React.useRef(null);
  const [phoneNumber, setPhoneNumber] = React.useState('');
  const [verificationId, setVerificationId] = React.useState('');
  const [verificationCode, setVerificationCode] = React.useState('');
  const firebaseConfig = firebase.apps.length ? firebase.app().options : undefined;
  const [message, showMessage] = React.useState(
    !firebaseConfig || Platform.OS === 'web'
      ? {
        text:
          'To get started, provide a valid firebase config in App.js and open this snack on an iOS or Android device.',
      }
      :
      undefined
  );
  const attemptInvisibleVerification = false;

  // const [visibleModalLoginVerify, setVisibleModalLoginVerify] = React.useState(false);
  // const showModalLoginVerify = () => setVisibleModalLoginVerify(true);

  return (
    <View style={{ padding: 20, marginTop: 50 }}>
      <FirebaseRecaptchaVerifierModal
        ref={recaptchaVerifier}
        firebaseConfig={firebaseConfig}
        attemptInvisibleVerification={attemptInvisibleVerification}
      />
      <Text style={{ marginTop: 20 }}>Enter phone number</Text>
      <TextInput
        style={{ marginVertical: 10, fontSize: 17 }}
        placeholder="+1 999 999 9999"
        autoFocus
        autoCompleteType="tel"
        keyboardType="phone-pad"
        textContentType="telephoneNumber"
        onChangeText={phoneNumber => setPhoneNumber(phoneNumber)}
      />
     <Button
        title="Send Verification Code"
        disabled={!phoneNumber}
        onPress={async () => {
          // The FirebaseRecaptchaVerifierModal ref implements the
          // FirebaseAuthApplicationVerifier interface and can be
          // passed directly to `verifyPhoneNumber`.
          try {
            console.log('pppp')
            const phoneProvider = new firebase.auth.PhoneAuthProvider();
            const verificationId = await phoneProvider.verifyPhoneNumber(
              phoneNumber,
              recaptchaVerifier.current
            );
            setVerificationId(verificationId);
            showMessage({
              text: 'Verification code has been sent to your phone.',
            });
          } catch (err) {
            showMessage({ text: `Error: ${err.message}` });
          }
        }}
      />
      <Text style={{ marginTop: 20 }}>Enter Verification code</Text>
      <TextInput
        style={{ marginVertical: 10, fontSize: 17 }}
        editable={!!verificationId}
        placeholder="123456"
        onChangeText={setVerificationCode}
      />
      <Button
        title="Confirm Verification Code"
        disabled={!verificationId}
        onPress={async () => {
          try {
            // console.log('uuu')
            // const recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
            // const appVerifier = recaptchaVerifier;
            // const phoneNumberString = "+" + phoneNumber;

            // firebase.auth().signInWithPhoneNumber(phoneNumberString, appVerifier)
            //   .then(confirmationResult => {
            //   })
            //   .catch(function (error) {
            //     console.error("SMS not sent", error);
            //   });

            // const credential = firebase.auth.PhoneAuthProvider.credential(
            //   verificationId,
            //   verificationCode
            // );
            // await firebase.auth().signInWithCredential(credential);
            // showMessage({ text: 'Phone authentication successful 👍' });
          } catch (err) {
            console.log(err.message)
            showMessage({ text: `Error: ${err.message}` });
          }
        }}
      />
      {message ? (
        <TouchableOpacity
          style={[
            StyleSheet.absoluteFill,
            // { backgroundColor: 0xffffffee, justifyContent: 'center' },
          ]}
          onPress={() => showMessage(undefined)}>
          <Text
            style={{
              // color: message.color || 'blue',
              fontSize: 17,
              textAlign: 'center',
              margin: 20,
            }}>
            {message.text}
          </Text>
        </TouchableOpacity>
      ) : (
        undefined
      )}
      {attemptInvisibleVerification && <FirebaseRecaptchaBanner />}
    </View>
  );
}


//   return (
//     <ImageBackground source={require('../../assets/headerPic2.jpg')} style={styles.imageBackground}>
//       <View style={styles.view}>
//         <Text style={styles.welcomeText}>Log in with phone number</Text>
//         <TextInput placeholder="Enter your phone number"
//           value={phoneNumber}
//           onChangeText={text => setPhoneNumber(text)}
//           style={styles.input} />
//         <TouchableOpacity style={styles.btnLogin}
//           onPress={showModalLoginVerify}>
//           <Text style={styles.btnLoginText}>Get verification code</Text>
//         </TouchableOpacity>
//         {visibleModalLoginVerify ? <ModalLoginVerify setVisibleModalLoginVerify={setVisibleModalLoginVerify} /> : null}
//       </View>
//     </ImageBackground>
//   )
// }

// const styles = StyleSheet.create({
//   view: {
//     backgroundColor: 'white',
//     padding: 60,
//     alignItems: 'center',
//     justifyContent: 'center',
//     borderRadius: 50
//   },
//   imageBackground: {
//     height: '101%',
//     width: '100%',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
//   welcomeText: {
//     fontSize: 25,
//     fontWeight: 'bold',
//     marginBottom: 10
//   },
//   btnLogin: {
//     justifyContent: 'center',
//     alignItems: 'center',
//     backgroundColor: 'black',
//     height: 35,
//     borderRadius: 50,
//     width: 130,
//     margin: 15
//   },
//   btnLoginText: {
//     color: 'white'
//   },
//   input: {
//     height: 40,
//     marginBottom: 5,
//     width: 200,
//     borderEndColor: 'gray',
//     borderWidth: 1,
//     padding: 5,
//     borderRadius: 5
//   },
//   button: {
//     alignItems: 'center',
//     backgroundColor: '#DDDDDD',
//     padding: 10,
//   },
// });