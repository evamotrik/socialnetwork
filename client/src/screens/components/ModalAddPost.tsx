import React, { useContext, useState } from 'react';
import { TouchableOpacity, Text, StyleSheet, View, Image, Modal, TextInput, ScrollView, Platform } from 'react-native';
import { Video } from 'expo-av';
import Context from '../../utils/context';
import { addPost, getMyPosts } from '../../services/http.service';
import * as ImagePicker from 'expo-image-picker';
import Ionicons from 'react-native-vector-icons/Ionicons';

export const ModalAddPost = ({ setVisibleAddPost, setPosts }: any) => {

    const context = useContext(Context);
    const [title, setTitle] = useState('');
    const [descr, setDescr] = useState('');
    const [img, setImg] = useState<any>(null);
    const [video, setVideo] = useState(null);

    const hideModalAddPost = async() => {
        const postsArr = await getMyPosts();
        setPosts(postsArr.posts);
        setVisibleAddPost(false);}
   

    const pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            allowsEditing: true,
            aspect: [4, 3],
            quality: 1,
        });

        if (result.cancelled === false && result && result.uri && result.uri !== '') {
            setImg(result.uri);
        }
    };

    const makeImage = async () => {
        let result = await ImagePicker.launchCameraAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            allowsEditing: true,
            aspect: [4, 3],
            quality: 1,
        });

        if (result.cancelled === false && result && result.uri && result.uri !== '') {
            setImg(result.uri);
        }
    };

    const vid = React.useRef(null);
    const [status, setStatus] = React.useState({});

    const makeVideo = async () => {
        let result = await ImagePicker.launchCameraAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.Videos,
            allowsEditing: true,
            aspect: [4, 3],
            quality: 1,
        });

        if (result.cancelled === false && result && result.uri && result.uri !== '') {
            setVideo(result.uri);
        }
    };

    async function btnAddNewPost() {
        const res = await addPost({ title, descr, img, video, user: context.userState._id });
        if (res) {
            hideModalAddPost()
        }
    }

    return (
        <Modal
            visible={true}
            onDismiss={hideModalAddPost}
            animationType="slide"
            transparent={true}>
            <View style={styles.modalAddPostBackground}>
                <View style={styles.modalView}>
                    <TouchableOpacity onPress={hideModalAddPost} style={styles.modalClose}>
                        <Ionicons name="close" size={30} color='black' />
                    </TouchableOpacity>
                    <Text style={styles.modalAddBtnText}>New Post</Text>
                    <TextInput
                        placeholder="Title"
                        value={title}
                        onChangeText={text => setTitle(text)}
                        style={styles.textInput} />
                    <TextInput
                        placeholder="Description"
                        value={descr}
                        onChangeText={text => setDescr(text)}
                        style={styles.textInput} />

                    <View style={styles.addImage}>
                        <TouchableOpacity onPress={pickImage}>
                            <Ionicons name="image-outline" size={35} color='black' />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={makeImage}>
                            <Ionicons name="camera-outline" size={35} color='black' />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={makeVideo}>
                            <Ionicons name="videocam-outline" size={35} color='black' />
                        </TouchableOpacity>
                    </View>
                    {img ? <Image source={{ uri: img }} style={styles.modalPic} /> : null}
                    {video ?
                        <Video
                            ref={vid}
                            style={styles.modalPic}
                            source={{ uri: video }}
                            useNativeControls
                            resizeMode="contain"
                            isLooping
                            onPlaybackStatusUpdate={status => setStatus(() => status)} />
                        : null}
                    <TouchableOpacity onPress={btnAddNewPost} style={styles.btnAddPost}>
                        <Text style={styles.btnAddPostText}>Add Post</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={hideModalAddPost} style={styles.btnCloseModal}>
                        <Text>Close</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    modalView: {
        height: '90%',
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        justifyContent: "center",
    },
    modalAddBtnText: {
        fontSize: 17,
        fontWeight: 'bold',
        marginBottom: 10
    },
    modalPic: {
        width: 150,
        height: 150,
        margin: 10
    },
    modalButtons: {
        flexDirection: "column",
        margin: 10
    },
    btnAddPostText: {
        color: 'white'
    },
    addImage: {
        flexDirection: "row"
    },
    btnCloseModal: {
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: 'black',
        borderWidth: 2,
        height: 35,
        borderRadius: 50,
        width: 130,
        marginBottom: 10
    },
    modalClose: {
        position: 'absolute',
        right: 5,
        top: 5,
        margin: 15,
        alignSelf: 'flex-end'
    },
    modalAddPostBackground: {
        flex: 1,
        backgroundColor: 'rgba(52, 52, 52, 0.8)',
    },
    modalBackground: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(52, 52, 52, 0.8)',
    },
    textInput: {
        height: 40,
        marginBottom: 5,
        width: 200,
        borderColor: 'gray',
        borderWidth: 1,
        padding: 5
    },
    btnAddPost: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'black',
        height: 35,
        borderRadius: 50,
        width: 130,
        marginBottom: 10,
    },
});