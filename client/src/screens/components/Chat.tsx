import React, { useEffect, useState, useContext, useRef } from 'react';
import { ImageBackground, Text, StyleSheet, View, Image, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import { goToChatWithFriend, socket } from '../../services/http.service';
import Context from '../../utils/context';

export const Chat = (props: any) => {

  const context = useContext(Context);

  const [messageText, setMessageText] = useState('');
  const [messages, setMessages] = useState([]);
  const scrollViewRef = useRef<any>();


  let friendId = props.route.params.friendId

  useEffect(() => {
   
      (async () => {
        const res = await goToChatWithFriend(friendId);
        setMessages(res.messages)
      })()
  }, [])

  useEffect(() => {
    socket.connect();
    socket.on('tweet', (messages: any) => {
      setMessages(messages)
    })
  }, [])


  const sendMessageBtn = async (mes: any) => {
    socket.emit('sendMessage', { userId: context.userState._id, userAvatar: context.userState.avatar, userSurname: context.userState.surname, userName: context.userState.name, messageText: messageText, friendId })
    setMessageText('')

  }

  useEffect(() => {
    scrollViewRef.current?.scrollToEnd({ animated: true })
  }, [messages.length])


  return (
    <View style={styles.view} >
      <ImageBackground source={require('../../../assets/headerPic.jpg')} style={styles.imageBackground}>
        <ScrollView ref={scrollViewRef}>
          <View style={styles.stylesMessages}>
            {
              messages.length > 0 ? messages.map(message => (
                <View key={message._id}>
                  {message.userId === context.userState._id ?
                    <View style={styles.myMessage}>
                      <View style={styles.messageTextMine}>
                        <Text>{message.messageText}</Text>
                      </View>
                    </View>
                    :
                    <View style={styles.friendMessage}>
                      <View style={styles.friendInfo}>
                        <Image
                          style={styles.avatar}
                          source={{ uri: message.userAvatar }}
                        />
                        <Text style={styles.friendsInfoText}>{message.userSurname} {message.userName}</Text>
                      </View>
                      <View style={styles.messageTextFriend}>
                        <Text>{message.messageText}</Text>
                      </View>
                    </View>
                  }

                </View>
              )) : null
            }
          </View>
        </ScrollView>
        <View style={styles.sendView}>
          <TextInput
            placeholder="Message..."
            value={messageText}
            onChangeText={text => setMessageText(text)}
            style={styles.input} />
          <TouchableOpacity style={styles.sendBtn} onPress={sendMessageBtn}>
            <Text style={styles.sendBtnText}>Send</Text>
          </TouchableOpacity>
        </View>
      </ImageBackground>
    </View>
  )
}

const styles = StyleSheet.create({
  view: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  imageBackground: {
    height: '101%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  stylesMessages: {
    marginTop: 10,
    width: 370,
    backgroundColor: 'white',
    borderRadius: 50,
    padding: 10,
    height: 900,
    justifyContent: 'flex-end',
  },
  avatar: {
    width: 27,
    height: 27,
    borderRadius: 100,
    borderColor: 'black',
    borderWidth: 2,
    borderBottomWidth: 0,
    left: 0,
    top: 0,
  },
  myMessage: {
    width: 270,
    alignSelf: 'flex-end',
    margin: 5
  },
  friendMessage: {
    width: 270,
    margin: 5,
  },
  friendInfo: {
    flexDirection: 'row',
    alignItems: 'center',
    margin: 5
  },
  friendsInfoText: {
    marginLeft: 5,
    fontSize: 13,
    fontWeight: 'bold'
  },
  messageTextFriend: {
    backgroundColor: '#D3D3D3',
    padding: 5,
    borderRadius: 50,
    borderWidth: 2,
    borderColor: 'black',
  },
  messageTextMine: {
    borderWidth: 2,
    borderColor: 'black',
    backgroundColor: 'white',
    padding: 5,
    borderRadius: 50,
  },
  sendView: {
    flexDirection: 'row',
    margin: 10
  },
  input: {
    height: 40,
    marginBottom: 5,
    width: 270,
    borderEndColor: 'gray',
    borderWidth: 1,
    padding: 10,
    borderRadius: 5,
    backgroundColor: 'white'
  },
  sendBtn: {
    backgroundColor: 'black',
    borderRadius: 50,
    width: 70,
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
    margin: 5,
  },
  sendBtnText: {
    color: 'white',
    fontWeight: 'bold'
  },
});


