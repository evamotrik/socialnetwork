import React, { useEffect, useState, useContext } from 'react';
import { Text, TextInput, StyleSheet, View, Image, ScrollView, TouchableOpacity, Modal } from 'react-native';
import { addComment, showPostComments, deleteComment } from '../../services/http.service';
import Context from '../../utils/context';
import Ionicons from 'react-native-vector-icons/Ionicons';

export const ModalComments = ({ postId, setVisibleComments }: any) => {
    const context = useContext(Context);
    const [comments, setComments] = useState([]);
    const [commentText, setCommentText] = useState('');
    const [commentId, setCommentId] = useState('');
    const [visibleDeleteComment, setVisibleDeleteComment] = React.useState(false);

    const hideModalDeleteComment = () => setVisibleDeleteComment(false);
    const showModalDeleteComment = (postId: any) => {
        setVisibleDeleteComment(true)
        setCommentId(postId)
    };

    const hideModalComments = () => setVisibleComments(false);

    const toDeleteComment = async () => {
        await deleteComment({ commentId, postId })
        hideModalComments()
    }

    async function addCommentBtn() {
        const comment = {
            commentText: commentText,
            postId: postId,
            userId: context.userState._id,
            userAvatar: context.userState.avatar,
            userSurname: context.userState.surname,
            userName: context.userState.name,
        }
        await addComment(comment);
        const commentsArr = await showPostComments({ postId });
        setComments(commentsArr);
        setCommentText('');
    }

    useEffect(() => {
        (async () => {
            const commentsArr = await showPostComments({ postId });
            setComments(commentsArr);
        })()
    }, [])

    return (
        <Modal
            visible={true}
            onDismiss={hideModalComments}
            animationType="slide"
            transparent={true}>
            <View style={styles.modalBackground}>
                <View style={styles.modalView}>
                    <TouchableOpacity onPress={hideModalComments} style={styles.modalClose}>
                        <Ionicons name="close" size={30} color='black' />
                    </TouchableOpacity>
                    <ScrollView>
                        <View style={styles.scrollComments}>
                            {
                                comments.length > 0 ? comments.map(comment => (
                                    <View key={comment._id} style={styles.comment}>
                                        {comment.userId === context.userState._id ?
                                            <TouchableOpacity onPress={() => showModalDeleteComment(comment._id)} style={styles.deleteCommentBtn}>
                                                <Ionicons name="close-circle-outline" size={25} color='black' />
                                            </TouchableOpacity> : null}
                                        <View style={styles.commentatorInfo}>
                                            <Image
                                                style={styles.commentatorAvatar}
                                                source={{ uri: comment.userAvatar }} />
                                            <View>
                                                <View style={styles.commentatorInfoText}>
                                                    <Text style={styles.commentatorInfoFont}>{comment.userName} {comment.userSurname}</Text>
                                                </View>
                                                <View style={styles.commentText}>
                                                    <Text>{comment.commentText}</Text>
                                                </View>
                                            </View>
                                        </View>
                                    </View>
                                )) : null
                            }
                        </View>
                    </ScrollView>
                    <TextInput
                        placeholder="Leave your comment"
                        value={commentText}
                        onChangeText={text => setCommentText(text)}
                        style={styles.textInput}
                    />
                    <TouchableOpacity onPress={addCommentBtn} style={styles.addCommentBtn}>
                        <Text style={styles.addCommentBtnText}>Add Comment</Text>
                    </TouchableOpacity>
                </View>

                <Modal visible={visibleDeleteComment}
                    onDismiss={hideModalDeleteComment}
                    animationType="slide"
                    transparent={true}>
                    <View style={styles.modalBackground}>
                        <View style={styles.modalDeleteComment}>
                            <TouchableOpacity onPress={hideModalDeleteComment} style={styles.modalClose}>
                                <Ionicons name="close" size={30} color='black' />
                            </TouchableOpacity>
                            <Text style={styles.modalText}>Are you sure you want to delete your comment?</Text>
                            <View style={styles.modalDeleteBtns}>
                                <TouchableOpacity onPress={toDeleteComment} style={styles.modalDeleteButtonYes}>
                                    <Text>Yes</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={hideModalDeleteComment} style={styles.modalDeleteButtonNo}>
                                    <Text style={styles.modalDeleteNoText}>No</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </Modal>

            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    modalView: {
        height: '90%',
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        justifyContent: "center",
    },
    scrollComments: {
        width: 300,
        margin: 5
    },
    comment: {
        justifyContent: 'flex-start',
        flexDirection: 'row',
        padding: 5,
        margin: 7,
        borderColor: 'black',
        borderWidth: 2,
        borderRadius: 20,
    },
    commentatorInfo: {
        flexDirection: 'row'
    },
    commentText: {
        margin: 5,
        justifyContent: 'center',
        width: 170
    },
    commentatorAvatar: {
        width: 40,
        height: 40,
        margin: 5,
        borderRadius: 100,
        borderWidth: 1,
        borderColor: 'black'
    },
    commentatorInfoText: {
        margin: 6,
        height: 10,
    },
    commentatorInfoFont: {
        fontSize: 12,
        fontWeight: 'bold'
    },
    textInput: {
        height: 40,
        marginBottom: 5,
        width: 300,
        borderColor: 'gray',
        borderWidth: 1,
        padding: 5,
        borderRadius: 5
    },
    addCommentBtn: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'black',
        height: 35,
        borderRadius: 50,
        width: 130,
        marginTop: 7
    },
    addCommentBtnText: {
        color: 'white',
    },
    deleteCommentBtn: {
        position: 'absolute',
        right: 0,
        top: 0,
        margin: 3,
        alignSelf: 'flex-end'
    },
    modalDeleteComment: {
        backgroundColor: 'white',
        height: 300,
        width: 350,
        borderRadius: 50,
        justifyContent: 'center',
        alignItems: 'center',
    },
    modalText:{
        fontSize: 15
    },
    modalDeleteBtns: {
        flexDirection: 'row',
        marginTop: 10
    },
    modalDeleteButtonYes: {
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: 'black',
        borderWidth: 2,
        height: 35,
        borderRadius: 50,
        width: 70,
        marginBottom: 10,
        marginRight: 10
    },
    modalDeleteButtonNo: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'black',
        height: 35,
        borderRadius: 50,
        width: 70,
        marginBottom: 10
    },
    modalDeleteNoText: {
        color: 'white'
    },
    modalBackground: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: 'rgba(52, 52, 52, 0.8)',
        justifyContent: 'center'
    },
    modalClose: {
        position: 'absolute',
        right: 5,
        top: 5,
        margin: 15,
        alignSelf: 'flex-end'
    },
})