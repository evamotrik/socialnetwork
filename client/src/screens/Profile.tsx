import React, { useContext, useEffect, useState } from 'react';
import { TouchableOpacity, ImageBackground, Text, StyleSheet, View, Image, Modal, TextInput, ScrollView, Platform } from 'react-native';
import { Video } from 'expo-av';
import Context from '../utils/context';
import { changeAvatar, getMyPosts, likePost, deletePost } from '../services/http.service';
import * as ImagePicker from 'expo-image-picker';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { ModalComments } from './components/ModalComments';
import { ModalAddPost } from './components/ModalAddPost';

export const Profile = () => {

    const context = useContext(Context);
    const [visibleAddPost, setVisibleAddPost] = React.useState(false);
    const [visibleDeletePost, setVisibleDeletePost] = React.useState(false);
    const [visibleComments, setVisibleComments] = React.useState(false);
    const [posts, setPosts] = useState([]);
    const [avatar, setAvatar] = useState<any>(context.userState.avatar);
    const [postId, setPostId] = useState("");
    const [postIdCommentModal, setPostIdCommentModal] = useState("");
    const ownerId = context.userState._id

    const vid = React.useRef(null);
    const [status, setStatus] = React.useState({});

    useEffect(() => {
        (async () => {
            const postsArr = await getMyPosts();
            setPosts(postsArr.posts);
            setAvatar(context.userState.avatar)
        })()
    }, [])

    useEffect(() => {
        (async () => {
            if (Platform.OS !== 'web') {
                const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
                if (status !== 'granted') {
                    alert('Sorry, we need camera roll permissions to make this work!');
                }
            }
        })
            ();
    }, []);

    const toLikePost = async (postId: any) => {
        await likePost({ postId })
        const postsArr = await getMyPosts();
        setPosts(postsArr.posts);
    }

    const toDeletePost = async () => {
        await deletePost({ postId })
        hideModalDeletePost();
        const postsArr = await getMyPosts();
        setPosts(postsArr.posts);
    }

    const showModalComments = (postId: any) => {
        setVisibleComments(true);
        setPostIdCommentModal(postId)
    }

    const showModalAddPost = () => setVisibleAddPost(true);

    const hideModalDeletePost = () => setVisibleDeletePost(false);

    const showModalDeletePost = (postId: any) => {
        setVisibleDeletePost(true)
        setPostId(postId)
    };

    const pickAvatar = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            allowsEditing: true,
            aspect: [4, 3],
            quality: 1,
        });

        if (result.cancelled === false && result && result.uri && result.uri !== '') {
            setAvatar(result.uri);
            await changeAvatar(result.uri);
        }
    };

    return (
        <View style={styles.view} >
            <ImageBackground source={require('../../assets/headerPic.jpg')} style={styles.profileHeader}>
                <TouchableOpacity onPress={pickAvatar}>
                    <Image
                        style={styles.avatar}
                        source={{ uri: avatar }}
                    />
                </TouchableOpacity>

                <View style={styles.userInfo}>
                    <Text style={styles.textUserInfo}>{context.userState.surname}</Text>
                    <Text style={styles.textUserInfo}>{context.userState.name}</Text>
                </View>
            </ImageBackground>



            <View style={styles.postsView}>
                <Text style={styles.welcomeText}>Hello there, {context.userState.name}!</Text>
                <TouchableOpacity onPress={showModalAddPost} style={styles.btnAddPost}>
                    <Text style={styles.btnAddPostText}>Add New Post</Text>
                </TouchableOpacity>

                <ScrollView>
                    <View style={styles.scroll}>
                        {
                            posts.length > 0 ? posts.map(post => (
                                <View key={post._id} style={styles.post}>
                                    {post.img ? <Image source={{ uri: post.img }} style={styles.postPic} /> : null}

                                    {post.video ?
                                        <View>
                                            <Video
                                                ref={vid}
                                                style={styles.postPic}
                                                source={{ uri: post.video }}
                                                useNativeControls
                                                resizeMode="contain"
                                                isLooping
                                                onPlaybackStatusUpdate={status => setStatus(() => status)} />
                                        </View>
                                        : null}
                                    <View style={styles.postInfo}>
                                        <Text style={styles.postTitle}>{post.title}</Text>
                                        <Text style={styles.postDesc}>{post.descr}</Text>
                                        <View style={styles.likesComments}>
                                            <View style={styles.likes}>
                                                <TouchableOpacity onPress={() => toLikePost(post._id)}>
                                                    {post.likes.includes(ownerId) == true ?
                                                        <Ionicons name="heart-circle" size={30} color='black' />
                                                        : <Ionicons name="heart-circle-outline" size={30} color='black' />
                                                    }

                                                </TouchableOpacity>
                                                <Text style={styles.likesNumb}>{post.likes.length}</Text>
                                            </View>
                                            <View style={styles.comments}>
                                                <TouchableOpacity onPress={() => showModalComments(post._id)}><Ionicons name="chatbubble-outline" size={30} color='black' /></TouchableOpacity>
                                            </View>
                                            <TouchableOpacity onPress={() => showModalDeletePost(post._id)}>
                                                <Ionicons name="trash-outline" size={30} color='black' />
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                            )) : <Text>You don't have any posts yet</Text>
                        }
                    </View>
                </ScrollView>
            </View>

            {visibleComments ? <ModalComments postId={postIdCommentModal} setVisibleComments={setVisibleComments} /> : null}
            {visibleAddPost ? <ModalAddPost setVisibleAddPost={setVisibleAddPost} setPosts={setPosts} /> : null}

            <Modal visible={visibleDeletePost}
                onDismiss={hideModalDeletePost}
                animationType="slide"
                transparent={true}>
                <View style={styles.modalBackground}>
                    <View style={styles.modalDeletePost}>
                        <TouchableOpacity onPress={hideModalDeletePost} style={styles.modalClose}>
                            <Ionicons name="close" size={30} color='black' />
                        </TouchableOpacity>
                        <Text style={styles.modalText}>Are you sure you want to delete this post?</Text>

                        <View style={styles.modalDeleteBtns}>
                            <TouchableOpacity onPress={toDeletePost} style={styles.modalDeleteButtonYes}>
                                <Text>Yes</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={hideModalDeletePost} style={styles.modalDeleteButtonNo}>
                                <Text style={styles.modalDeleteNoText}>No</Text>
                            </TouchableOpacity>
                        </View>

                    </View>
                </View>

            </Modal>
        </View>
    )
}

const styles = StyleSheet.create({
    view: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    profileHeader: {
        flex: 2 / 7,
        width: '100%',
        flexDirection: 'row',
    },
    userInfo: {
        margin: 20,
        borderRadius: 20,
        backgroundColor: 'white',
        padding: 10,
        width: 200,
        borderColor: 'black',
        borderWidth: 5,
        borderStyle: 'dashed',
        alignItems: 'center',
        justifyContent: 'center'
    },
    textUserInfo: {
        fontSize: 25,
        textAlign: 'center',
        fontWeight: 'bold',
        color: 'black',
    },
    avatar: {
        width: 120,
        height: 120,
        margin: 10,
        marginLeft: 25,
        borderRadius: 100,
        borderColor: 'black',
        borderWidth: 4,
        borderBottomWidth: 0
    },
    welcomeText: {
        fontSize: 25
    },
    scroll: {
        padding: 10,
        alignItems: 'center',
    },

    btnAddPost: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'black',
        height: 35,
        borderRadius: 50,
        width: 130,
        marginBottom: 10,
    },
    btnAddPostText: {
        color: 'white'
    },

    postsView: {
        width: '100%',
        flex: 3 / 4,
        alignItems: 'center',
        borderWidth: 4,
        borderTopColor: 'black',
        borderTopStartRadius: 50,
        borderTopEndRadius: 50,
    },
    post: {
        marginBottom: 25,
        alignItems: 'center',
        width: 400
    },
    postInfo:{
        width: 300,
        margin: 10,
        alignItems: 'flex-start'
    },
    postTitle: {
        fontSize: 15,
        fontWeight: 'bold'
    },
    postDesc: {
        fontSize: 15
    },
    postPic: {
        width: 300,
        height: 300,
        borderRadius: 30
    },
    likesComments: {
        alignItems: 'center',
        flexDirection: 'row'
    },
    likes: {
        alignItems: 'center',
        flexDirection: 'row'
    },
    comments: {
        alignItems: 'center',
        flexDirection: 'row'
    },
    likesNumb: {
        fontSize: 15,
        marginRight: 5
    },
    modalDeletePost: {
        backgroundColor: 'white',
        height: 300,
        width: 350,
        borderRadius: 50,
        justifyContent: 'center',
        alignItems: 'center',
    },
    modalText: {
        fontSize: 16
    },
    modalDeleteBtns: {
        flexDirection: 'row',
        marginTop: 10
    },
    modalDeleteButtonYes: {
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: 'black',
        borderWidth: 2,
        height: 35,
        borderRadius: 50,
        width: 70,
        marginBottom: 10,
        marginRight: 10
    },
    modalDeleteButtonNo: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'black',
        height: 35,
        borderRadius: 50,
        width: 70,
        marginBottom: 10
    },
    modalDeleteNoText: {
        color: 'white'
    },
    modalClose: {
        position: 'absolute',
        right: 5,
        top: 5,
        margin: 15,
        alignSelf: 'flex-end'
    },
    modalBackground: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(52, 52, 52, 0.8)',
    },

});


