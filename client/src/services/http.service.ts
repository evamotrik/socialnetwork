import { Alert } from 'react-native';
import { storeTokenInfo } from './asyncStorage.service';
import { getTokenInfo } from './asyncStorage.service';
import { validationSchemaLogin } from '../validation/validationLogin';
import { validationSchemaRegistration } from '../validation/validationRegistration';
import { io } from "socket.io-client";


const URL = 'http://192.168.1.132:3000';
export const socket = io(URL, { autoConnect: false });

export async function registration(data: any) {
    try {
        await validationSchemaRegistration.validate(data)
        const res = await fetch(`${URL}/auth/registration`, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        })
        if (res.status === 200) {
            return true
        } 
        else if(res.status === 401){
            Alert.alert('Error', 'User with this email already exist');
            return false;
        }
        else {
            Alert.alert('Error', 'Something went wrong... Server is not answering!!!');
            return false;
        }
    } catch (e) {
        Alert.alert(e.message)
    }
}

export async function logIn(data: any) {
    try {
        await validationSchemaLogin.validate(data)
        const res = await fetch(`${URL}/auth/login`, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        });
        if (res.status == 200) {
            const json = await res.json();
            storeTokenInfo({
                accessToken: json.token,
            });
            return json;
        }
        else if(res.status === 401){
            Alert.alert('Error', 'User with this email not found');
            return false;
        }
        else if(res.status === 402){
            Alert.alert('Error', 'Incorrect password');
            return false;
        }
        else {
            Alert.alert('Error', 'Something went wrong... Server is not answering!!!');
            return false;
        }
    } catch (e) {
        Alert.alert(e.message)
    }
}

export async function addPost(data: any) {
    const tokenInfo = await getTokenInfo();
    const res = await fetch(`${URL}/post/create`, {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            'Authorization': `Bearer ${tokenInfo.accessToken}`,
            'Content-Type': 'application/json'
        }
    });
    const json = await res.json();
    return json;
}

export async function deletePost(postId: any) {
    const tokenInfo = await getTokenInfo();
    const res = await fetch(`${URL}/post/delete`, {
        method: "DELETE",
        body: JSON.stringify(postId),
        headers: {
            'Authorization': `Bearer ${tokenInfo.accessToken}`,
            'Content-Type': 'application/json'
        }
    });
    if(res.status === 200) {
        return
    }
    return false;
}

export async function getMyPosts() {
    const tokenInfo = await getTokenInfo();
    const res = await fetch(`${URL}/post/getMyPosts`, {
        method: "GET",
        headers: {
            'Authorization': `Bearer ${tokenInfo.accessToken}`,
            'Content-Type': 'application/json'
        }
    })
    const json = await res.json();
    return json
}

export async function likePost(postId: any) {
    const tokenInfo = await getTokenInfo();
    const res = await fetch(`${URL}/allUsers/likePost`, {
        method: "POST",
        body: JSON.stringify(postId),
        headers: {
            'Authorization': `Bearer ${tokenInfo.accessToken}`,
            'Content-Type': 'application/json'
        }
    });
    if(res.status === 200) {
        return
    }
    return false;
}

export async function addComment(data: any) {
    const tokenInfo = await getTokenInfo();
    const res = await fetch(`${URL}/allUsers/addComment`, {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            'Authorization': `Bearer ${tokenInfo.accessToken}`,
            'Content-Type': 'application/json'
        }
    });
    const json = await res.json();
    return json;
}

export async function deleteComment(data: any) {
    const tokenInfo = await getTokenInfo();
    const res = await fetch(`${URL}/allUsers/deleteComment`, {
        method: "DELETE",
        body: JSON.stringify(data),
        headers: {
            'Authorization': `Bearer ${tokenInfo.accessToken}`,
            'Content-Type': 'application/json'
        }
    });
    if(res.status === 200) {
        return
    }
    return false;
}

export async function showPostComments(postId: any) {
    const tokenInfo = await getTokenInfo();
    const res = await fetch(`${URL}/allUsers/showPostComments`, {
        method: "POST",
        body: JSON.stringify(postId),
        headers: {
            'Authorization': `Bearer ${tokenInfo.accessToken}`,
            'Content-Type': 'application/json'
        }
    });
    const json = await res.json();
    return json;
}


export async function changeAvatar(avatar: any) {
    const tokenInfo = await getTokenInfo();
    const res = await fetch(`${URL}/profile/changeAvatar`, {
        method: "POST",
        body: JSON.stringify({ avatar }),
        headers: {
            'Authorization': `Bearer ${tokenInfo.accessToken}`,
            'Content-Type': 'application/json'
        }
    });
    const json = await res.json();
    return json;
}

export async function getAllUsers() {
    try {
        const res = await fetch(`${URL}/allUsers/getAllUsers`, {
            method: "GET",
            headers: {
                'Content-Type': 'application/json'
            }
        })
        const json = await res.json();
        return json
    } catch (e) {
        Alert.alert(e.message)
    }
}

export async function addFriend(friendsId: any) {
    const tokenInfo = await getTokenInfo();
    const res = await fetch(`${URL}/friends/addFriend`, {
        method: "POST",
        body: JSON.stringify(friendsId),
        headers: {
            'Authorization': `Bearer ${tokenInfo.accessToken}`,
            'Content-Type': 'application/json'
        }
    });
    const json = await res.json();
    return json;
}

export async function sendInvitation(friendsId: any) {
    const tokenInfo = await getTokenInfo();
    const res = await fetch(`${URL}/friends/sendInvitation`, {
        method: "POST",
        body: JSON.stringify(friendsId),
        headers: {
            'Authorization': `Bearer ${tokenInfo.accessToken}`,
            'Content-Type': 'application/json'
        }
    });
    const json = await res.json();
    return json;
}

export async function showInvitation() {
    try {
        const tokenInfo = await getTokenInfo();
        const res = await fetch(`${URL}/friends/showInvitation`, {
            method: "GET",
            headers: {
                'Authorization': `Bearer ${tokenInfo.accessToken}`,
                'Content-Type': 'application/json'
            }
        })
        const json = await res.json();
        return json
    } catch (e) {
        Alert.alert(e.message)
    }
}

export async function deleteInvitation(friendsId: any) {
    const tokenInfo = await getTokenInfo();
    const res = await fetch(`${URL}/friends/deleteInvitation`, {
        method: "DELETE",
        body: JSON.stringify(friendsId),
        headers: {
            'Authorization': `Bearer ${tokenInfo.accessToken}`,
            'Content-Type': 'application/json'
        }
    });
    if(res.status === 200) {
        return
    }
    return false;
}

export async function showAllFriends() {
    try {
        const tokenInfo = await getTokenInfo();
        const res = await fetch(`${URL}/friends/showAllFriends`, {
            method: "GET",
            headers: {
                'Authorization': `Bearer ${tokenInfo.accessToken}`,
                'Content-Type': 'application/json'
            }
        })
        const json = await res.json();
        return json
    } catch (e) {
        Alert.alert(e.message)
    }
}

export async function deleteFromFriends(friendId: any) {
    const tokenInfo = await getTokenInfo();
    const res = await fetch(`${URL}/friends/delete`, {
        method: "DELETE",
        body: JSON.stringify(friendId),
        headers: {
            'Authorization': `Bearer ${tokenInfo.accessToken}`,
            'Content-Type': 'application/json'
        }
    });
    if(res.status === 200) {
        return
    }
    return false;
}

export async function goToChatWithFriend(friendId: any) {
    const tokenInfo = await getTokenInfo();
    const res = await fetch(`${URL}/chat/getChat`, {
        method: "POST",
        body: JSON.stringify({friendId}),
        headers: {
            'Authorization': `Bearer ${tokenInfo.accessToken}`,
            'Content-Type': 'application/json'
        }
    });
    const json = await res.json();
    return json;
}





