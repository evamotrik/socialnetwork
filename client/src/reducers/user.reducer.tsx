import { SHOW_INFO } from "../constants/user.constant";
export const defaultState = {
}

export const ReducerFunction = (state = defaultState, action: any) => {
    switch (action.type) {
        case SHOW_INFO:
            let newState = {
                ...state, ...action.data
            };
            return newState;
        default:
            return state;
    }

};
