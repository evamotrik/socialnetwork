import React from "react";
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NavigationContainer } from '@react-navigation/native';
import { Registration } from '../screens/Registration';
import { Login } from '../screens/Login';
import { LoginWithPhone } from '../screens/LoginWithPhone';
import { Chat } from '../screens/components/Chat';
import BottomTabNavigator from './TabNavigator'

const Stack = createNativeStackNavigator();

export default function StackNavigator() {
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName="Login"
                screenOptions={{
                    headerStyle: {
                        backgroundColor: "white",
                    },
                    headerTintColor: "black",
                    headerBackTitle: "Back",
                }}>
                <Stack.Screen name="Registration" component={Registration} initialParams={{ isRegistered: false }} />
                <Stack.Screen name="Login" component={Login} />
                <Stack.Screen name="LoginWithPhone" component={LoginWithPhone} />
                <Stack.Screen name="Chat" component={Chat}
                 />
                <Stack.Screen name="Profile" component={BottomTabNavigator}
                     options={{
                        headerShown: false,
                    }}
                />
            </Stack.Navigator>
        </NavigationContainer>
    )
}


