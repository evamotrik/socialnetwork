import React, { useContext } from "react";
import { TouchableOpacity } from 'react-native';
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { Profile } from '../screens/Profile';
import { Users } from '../screens/Users';
import { Chats } from '../screens/Chats';
import { Main } from '../screens/Main';
import Context from "../utils/context";
import { getFocusedRouteNameFromRoute } from "@react-navigation/core";

import Ionicons from 'react-native-vector-icons/Ionicons';

const Tab = createBottomTabNavigator();

export default function BottomTabNavigator({ navigation, route }) {

    const context = useContext(Context);

    function getHeaderTitle(route: any) {
        const routeName = getFocusedRouteNameFromRoute(route) ?? 'Profile';
        switch (routeName) {
            case 'Profile':
                return context.userState.surname + " " + context.userState.name;
        }
    }

    return (
       
        <Tab.Navigator
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                    let iconName;
                    if (route.name === 'Profile') {
                        iconName = focused
                            ? 'person'
                            : 'person-outline';
                    } else if (route.name === 'Users') {
                        iconName = focused ? 'search' : 'search-outline';
                    }
                    else if (route.name === 'Chats') {
                        iconName = focused ? 'chatbubble' : 'chatbubble-outline';
                    }
                    else if (route.name === 'Main') {
                        iconName = focused ? 'home' : 'home-outline';
                    }
                    else if (route.name === 'Friends') {
                        iconName = focused ? 'people' : 'people-outline';
                    }

                    return <Ionicons name={iconName} size={size} color={color} />;
                },
                tabBarActiveTintColor: 'black',
                tabBarInactiveTintColor: 'gray',
            })}
        >
            <Tab.Screen name="Profile" component={Profile}
                options={({ navigation, route, }) => ({
                    headerRight: () => (
                        <TouchableOpacity
                            onPress={() => navigation.navigate('Login')}>
                            <Ionicons name="log-out-outline" size={30} color='black' />
                        </TouchableOpacity>
                    ),
                    headerTitle: getHeaderTitle(route)
                })}
            />
            <Tab.Screen name="Chats" component={Chats}
                options={({ navigation, route, }) => ({
                    headerRight: () => (
                        <TouchableOpacity
                            onPress={() => navigation.navigate('Login')}>
                            <Ionicons name="log-out-outline" size={30} color='black' />
                        </TouchableOpacity>
                    ),
                    headerTitle: getHeaderTitle(route)
                })} />
            <Tab.Screen name="Main" component={Main}
                options={({ navigation, route, }) => ({
                    headerRight: () => (
                        <TouchableOpacity
                            onPress={() => navigation.navigate('Login')}>
                            <Ionicons name="log-out-outline" size={30} color='black' />
                        </TouchableOpacity>
                    ),
                    headerTitle: getHeaderTitle(route)
                })} />

            <Tab.Screen name="Users" component={Users}
                options={({ navigation, route, }) => ({
                    headerRight: () => (
                        <TouchableOpacity
                            onPress={() => navigation.navigate('Login')}>
                            <Ionicons name="log-out-outline" size={30} color='black' />
                        </TouchableOpacity>
                    ),
                    headerTitle: getHeaderTitle(route)
                })}
            />

        </Tab.Navigator>
    );
};

